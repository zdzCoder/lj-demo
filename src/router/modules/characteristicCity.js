import layout from '@/layout/index.vue'
export default [{
		path: '/characteristicCity',
		name: '用户管理222',
		component: layout,
		redirect: '/characteristicCity/userManage',
		children: [{
			path: '/characteristicCity/userManage',
			name: 'userManage222',
			meta: {
				title: '用户管理',
				icon: 'home'
			},
			component: (resolve) => require(['@/pages/characteristicCity/userManage'], resolve),
		}]
	},
	{
			path: '/characteristicCity',
			name: '业务预警',
			component: layout,
			redirect: '/characteristicCity/businessWarning',
			children: [{
				path: '/characteristicCity/businessWarning',
				name: 'businessWarning',
				meta: {
					title: '业务预警',
					icon: 'home'
				},
				component: (resolve) => require(['@/pages/characteristicCity/businessWarning'], resolve),
			}]
		}
]
