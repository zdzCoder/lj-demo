import layout from '@/layout/index.vue'
export default [{
		path: '/garbage',
		name: '用户管理333',
		component: layout,
		redirect: '/garbage/userManage',
		children: [{
			path: '/garbage/userManage',
			name: 'userManage',
			meta: {
				title: '用户管理',
				icon: 'home'
			},
			component: (resolve) => require(['@/pages/garbage/userManage'], resolve),
		}]
	},

	{
		path: '/garbage',
		name: '基础信息管理',
		component: layout,
		redirect: '/garbage/baseInfoManage',
		children: [{
			path: '/garbage/baseInfoManage',
			name: 'baseInfoManage',
			meta: {
				title: '基础信息管理',
				icon: 'home'
			},
			component: (resolve) => require(['@/pages/garbage/baseInfoManage'], resolve),
		}]
	},

	{
		alwaysShow: true,
		path: '/regionManage',
		name: 'regionManage',
		component: layout,
		meta: {
			title: '省市级监控管理',
			icon: 'home'
		},
		children: [{
				path: '/regionManage/domesticWaste',
				name: 'domesticWaste',
				alwaysShow: true,
				meta: {
					title: '农村生活垃圾源头管理'
				},
				component: resolve => require(['@/pages/routerView'], resolve),
				children: [{
					path: '/regionManage/domesticWaste/tzSearch',
					name: 'tzSearch',
					component: resolve => require(['@/pages/garbage/regionManage/domesticWaste/tzSearch'], resolve),
					meta: {
						title: '台账查询'
					}
				}, {
					path: '/regionManage/domesticWaste/tbSearch',
					name: 'tbSearch',
					component: resolve => require(['@/pages/garbage/regionManage/domesticWaste/tbSearch'], resolve),
					meta: {
						title: '垃圾填报查询'
					}
				}, {
					path: '/regionManage/domesticWaste/gisShow',
					name: 'gisShow',
					component: resolve => require(['@/pages/garbage/regionManage/domesticWaste/gisShow'], resolve),
					meta: {
						title: '农村生活垃圾GIS展示'
					}
				}],
			},
			{
				path: '/regionManage/collectManage',
				name: 'collectManage',
				component: resolve => require(['@/pages/garbage/regionManage/collectManage'], resolve),
				meta: {
					title: '垃圾收集监管',
					icon: 'home'
				}
			},
			{
				path: '/regionManage/transferStationManage',
				name: 'transferStationManage',
				alwaysShow: true,
				meta: {
					title: '垃圾中转站点监管'
				},
				component: resolve => require(['@/pages/routerView'], resolve),
				children: [{
						path: '/regionManage/transferStationManage/archivesSearch',
						name: 'archivesSearch',
						component: resolve => require(['@/pages/garbage/regionManage/transferStationManage/archivesSearch'], resolve),
						meta: {
							title: '中转站计量查询'
						}
					}, {
						path: '/regionManage/transferStationManage/archivesSearchGISManage',
						name: 'archivesSearchGISManage',
						component: resolve => require(['@/pages/garbage/regionManage/transferStationManage/archivesSearchGISManage'],
							resolve),
						meta: {
							title: '中转站GIS监管'
						}
					}, {
						path: '/regionManage/transferStationManage/meteringSearch',
						name: 'meteringSearch',
						component: resolve => require(['@/pages/garbage/regionManage/transferStationManage/meteringSearch'], resolve),
						meta: {
							title: '垃圾站档案查询'
						}
					},
					{
						path: '/regionManage/transferStationManage/videoSearch',
						name: 'videoSearch',
						component: resolve => require(['@/pages/garbage/regionManage/transferStationManage/videoSearch'], resolve),
						meta: {
							title: '中转站视频监控查询'
						}
					}
				],
			},
			{
				path: '/regionManage/endManage',
				name: 'endManage',
				alwaysShow: true,
				meta: {
					title: '垃圾末端处置监管'
				},
				component: resolve => require(['@/pages/routerView'], resolve),
				children: [{
						path: '/regionManage/endManage/endMeteringManage',
						name: 'endMeteringManage',
						component: resolve => require(['@/pages/garbage/regionManage/endManage/endMeteringManage'], resolve),
						meta: {
							title: '末端处置设施计量监管'
						}
					}, {
						path: '/regionManage/endManage/endMeteringVideoManage',
						name: 'endMeteringVideoManage',
						component: resolve => require(['@/pages/garbage/regionManage/endManage/endMeteringVideoManage'], resolve),
						meta: {
							title: '末端处置设施视频监管'
						}
					}, {
						path: '/regionManage/endManage/facilityEnvironmentManage',
						name: 'facilityEnvironmentManage',
						component: resolve => require(['@/pages/garbage/regionManage/endManage/facilityEnvironmentManage'], resolve),
						meta: {
							title: '设施环境监管'
						}
					},
					{
						path: '/regionManage/endManage/managementMeteringInManage',
						name: 'managementMeteringInManage',
						component: resolve => require(['@/pages/garbage/regionManage/endManage/managementMeteringInManage'], resolve),
						meta: {
							title: '设施处置中控数据监管'
						}
					}
				],
			},
		],
	},
	{
		path: '/garbage',
		name: '统计分析',
		component: layout,
		redirect: '/garbage/statisticalAnalysis',
		children: [{
			path: '/garbage/statisticalAnalysis',
			name: 'statisticalAnalysis',
			meta: {
				title: '统计分析',
				icon: 'home'
			},
			component: (resolve) => require(['@/pages/garbage/statisticalAnalysis'], resolve),
		}]
	},
	{
		path: '/garbage',
		name: '考核评价',
		component: layout,
		redirect: '/garbage/evaluate',
		children: [{
			path: '/garbage/evaluate',
			name: 'evaluate',
			meta: {
				title: '考核评价',
				icon: 'home'
			},
			component: (resolve) => require(['@/pages/garbage/evaluate'], resolve),
		}]
	}





]
