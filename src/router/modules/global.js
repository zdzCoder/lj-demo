import layout from '@/layout/index.vue'
export default [{
		path: '/',
		name: 'layout',
		component: layout,
		redirect: '/index',
		children: [{
			path: '/index',
			name: 'index',
			meta: {
				title: '首页',
				icon: 'home'
			},
			component: () => import('@/pages'),
		}]
	},
	{
		path: '/login',
		name: 'login',
		component: () => import('@/pages/login.vue'),
		hidden: true,
	},
	{
		path: '/404',
		name: 'pageNotFind',
		component: () => import('@/pages/404.vue'),
		hidden: true,
	}
]
