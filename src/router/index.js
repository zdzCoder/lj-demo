import Vue from 'vue'
import Router from 'vue-router'
import garbage from '@/router/modules/garbage' //农村生活垃圾治理信息子系统路由
import sewageDisposal from '@/router/modules/sewageDisposal' //污水处理设施建设信息子系统路由
import characteristicCity from '@/router/modules/characteristicCity' //特色小城镇培育子系统路由
import global from '@/router/modules/global' //全局路由
Vue.use(Router)
 

//路由注册 暂时先注册全部路由 后期根据登陆人权限判断 加载路由
export const routes = [
	...global,
	...garbage,
	...sewageDisposal,
	...characteristicCity
]

const router = new Router({
	mode: 'history',
	routes,
})

router.beforeEach((to, from, next) => {
  if (to.matched.length ===0) {   
    next({ path:'/404' })
  } else {
    next();
  }
});

//消除路由重复加载的报错
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
	return originalPush.call(this, location).catch(err => err)
}
export default router
