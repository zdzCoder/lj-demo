import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import component from './components'
import router from './router'
import store from './store'
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(component)
import '@/assets/css/reset.css'
import '@/assets/css/public.css'
import '@/assets/icons'
new Vue({
    router,
		store,
    render: h => h(App),
}).$mount('#app')