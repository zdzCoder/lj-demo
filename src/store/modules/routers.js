import global from '@/router/modules/global' //全局路由
import garbage from '@/router/modules/garbage' //农村生活垃圾治理信息子系统路由
import sewageDisposal from '@/router/modules/sewageDisposal' //污水处理设施建设信息子系统路由
import characteristicCity from '@/router/modules/characteristicCity' //特色小城镇培育子系统路由

const routers = {
	state: {
		routers: [],
		status:0
	},

	mutations: {
		SET_ROUTERS: (state, routerFlag) => {
			console.log(routerFlag)
			/*
			  routerFlag ==='1' 添加农村生活垃圾治理信息子系统路由
				routerFlag ==='2' 添加污水处理设施建设信息子系统路由
				routerFlag ==='3' 添加特色小城镇培育子系统路由
			*/
			state.status = routerFlag
			sessionStorage.routerFlag = routerFlag
			if (routerFlag === '1') {
        state.routers=[...global,...garbage]
			} else if (routerFlag === '2') {
				state.routers=[...global,...sewageDisposal]			
			} else {
        state.routers=[...global,...characteristicCity]
			}
			console.log(state.routers)
		}
	},

	actions: {

	}
}



export default routers
